Hostess will find any file that is in either the same directory as the executable, or in any folder (and sub-folder) located at the same level as Hostess.
```
#!text

Hostess(folder)
 |- hostess.exe
 |- MSphere.hosts
 '- Favorites(folder)
     |- Special Server.hosts
     '- TestServer1.hosts
```    
In this case MSphere.hosts, Special Server.hosts and TestServer1.hosts will all show in the host file listing.