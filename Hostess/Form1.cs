﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hostess
{
    public partial class Form1 : Form
    {
        public Dictionary<string, string> hosts = new Dictionary<string, string>();
        Timer directorTimer = new Timer();

        bool indexChangedBecauseOfUser = true;

        public Form1()
        {
            InitializeComponent();
            directorTimer.Interval = 1000;
            directorTimer.Tick += directorTimer_Tick;
            directorTimer.Enabled = true;
            refreshList();
        }

        void directorTimer_Tick(object sender, EventArgs e)
        {
            indexChangedBecauseOfUser = false;
            if ((lstHosts.Items.Count > 0) && (lstHosts.SelectedIndex != -1))
            {
                var t = lstHosts.Text;
                refreshList();
                lstHosts.SelectedIndex = lstHosts.FindString(t);
            }
            else
                refreshList();
            indexChangedBecauseOfUser = true;
        }

        public bool refreshList()
        {
            var count = lstHosts.Items.Count;
            int c = 0;
            foreach (var item in Directory.GetFiles(@"hosts", "*.hosts", SearchOption.AllDirectories))
                c++;

            if (count != c)
            {

                lstHosts.Items.Clear();

                foreach (var item in Directory.GetFiles(@"hosts", "*.hosts", SearchOption.AllDirectories))
                    lstHosts.Items.Add(item);
            }
            return true;
        }

        private void uSEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\drivers\etc\hosts"))
                sw.Write(txtHost.Text);
            using (StreamReader sr = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\drivers\etc\hosts"))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Hosts file set.");
                sb.AppendLine("The following has been read from your now current hosts file:");
                sb.AppendLine();
                sb.AppendLine(sr.ReadToEnd());
                MessageBox.Show(sb.ToString());
            }

        }
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAdd newHosts = new frmAdd();
            newHosts.Show();
        }
        private void deleteSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lstHosts.SelectedIndex != -1)
            {
                var delete = MessageBox.Show("Do you want to delete the hosts file:\n\n" + lstHosts.Text, "caption", MessageBoxButtons.YesNo);
                if ((delete == DialogResult.Yes) && File.Exists(lstHosts.Text))
                {
                    File.Delete(lstHosts.SelectedItem.ToString());
                    refreshList();
                    txtHost.Clear();
                }
            }
        }
        private void lstHosts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((lstHosts.SelectedIndex != -1) && indexChangedBecauseOfUser)
                using (StreamReader sr = new StreamReader(lstHosts.Text))
                    txtHost.Text = sr.ReadToEnd();
        }
        private void lolwutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Q: What is this?");
            sb.AppendLine("\tThis tool is made to help you switch between hosts files quickly");
            sb.AppendLine("");
            sb.AppendLine("Q: How to use?");
            sb.AppendLine("\tClick on the hosts file you want at the top, it will show in the bottom. Make any necessary changes in the bottom. When ready to set, click the USE item and it will be set to your hosts file.");
            sb.AppendLine("");
            sb.AppendLine("Where is my hosts file?");
            sb.AppendLine("\tYours should be located at:");
            sb.AppendLine("\t" + Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\drivers\etc\hosts");
            sb.AppendLine("");
            sb.AppendLine("Q: What is the file structure?");
            sb.AppendLine("\tAnything in the \"hosts\" directory next to the exe (including sub-directories) with the extension \".hosts\" will get picked up by Hostess. The directories are just for organiztion, and you can use whatever you want.");

            MessageBox.Show(sb.ToString());
        }

        private void ListDirectory(TreeView treeView, string path)
        {
            treeView.Nodes.Clear();
            var rootDirectoryInfo = new DirectoryInfo(path);
            treeView.Nodes.Add(CreateDirectoryNode(rootDirectoryInfo));
        }

        private static TreeNode CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            var directoryNode = new TreeNode(directoryInfo.Name);
            foreach (var directory in directoryInfo.GetDirectories())
                directoryNode.Nodes.Add(CreateDirectoryNode(directory));
            foreach (var file in directoryInfo.GetFiles())
                directoryNode.Nodes.Add(new TreeNode(file.Name));
            return directoryNode;
        }
    }
}
